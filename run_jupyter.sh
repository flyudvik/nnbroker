#!/usr/bin/env bash

docker-compose -f local.yml run -p 8888:8888 django jupyter notebook --ip=0.0.0.0 --allow-root
