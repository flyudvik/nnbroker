# Installation guide
Requirements:
* Linux OS, but Windows 10 and macOS are possible too
* docker
* docker-compose
* 512MB of RAM
* 5GB of ROM
* **INTERNET**
* Available ports of `8000` and `8025`

This repository uses docker. https://www.docker.com/

Firstly you need to install docker on your local machine. 
Visit https://docs.docker.com/engine/installation/

After installation of docker install docker-compose. 
Visit https://docs.docker.com/compose/install/

Now you can build the image:
```bash
docker-compose -f local.yml build
```

Then you can start the application after successful build
```bash
docker-compose -f local.yml run django python manage.py migrate

# Create superuser to be able to access the admin page
docker-compose -f local.yml run django python manage.py createsuperuser

docker-compose -f local.yml up -d
```

Then visit `http://localhost:8000` and try to sign in.

Visit `http://localhost:8025` to activate your account.

In `http://localhost:8000/admin/` you will find the application

Also you need to train the models manually and upload that models to database through admin page.


Good luck on using this product.
