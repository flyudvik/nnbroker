import logging
from collections import Counter

import numpy as np
from celery._state import connect_on_app_finalize
from celery.schedules import crontab
from django.conf import settings
# from django.dispatch import receiver
# from celery.signals import worker_ready

from .models import ExchangeBot, CompanyProcessingRule, ExchangeStrategy
from .utils.mocker.workflow import PredictionWorkFlow
from nnbroker.taskapp.celery import app

logger = logging.getLogger('predictions:tasks')


# @receiver(worker_ready)
@connect_on_app_finalize
def setup_periodic_tasks(sender, **kwargs):
    for f_rule in CompanyProcessingRule.objects.prefetch_related('bot').all():
        seconds = f_rule.bot.period_rate
        sender.add_periodic_task(
            seconds,
            run_predictions.s(f_rule.id, f_rule.bot.id, f_rule.stock, debug=settings.DEBUG),
            name=f_rule
        )


# @app.periodic_task(run_every=crontab(minute=settings.DEFAULT_RATE))
# def run_default_rate():
#     rules = CompanyProcessingRule.objects.filter(
#         stock__bot__period_rate=settings.DEFAULT_RATE,
#         stock__actual_money__gt=0
#     )
#     running_strategies = ExchangeStrategy.objects.filter(
#         rule__in=rules,
#         status=ExchangeStrategy.RUNNING,
#     )


@app.task()
def run_predictions(instance_id, bot_id, stock_id, debug):
    if not debug:
        logger.error("It is not ready for production use")
        return
    try:
        bot_instance = ExchangeBot.objects.get()
    except ExchangeBot.DoesNotExist:
        logger.error("Exchange bot with id: {} does not exist".format(bot_id))
        return
    logger.info("[#] Running prediction [#]")



    logger.info('[*] Exchange action is DONE [*]')


@app.task()
def run_action():
    pass
