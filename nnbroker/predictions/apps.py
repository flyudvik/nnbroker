from django.apps import AppConfig


class PredictionsConfig(AppConfig):
    name = 'nnbroker.predictions'
    verbose_name = "Prediction application"

    def ready(self):
        """Override this to put in:
            Users system checks
            Users signal registration
        """
        from . import signals  # noqa
