import numpy as np

SELL = 0
BUY = 1


def reversed_enumerate(seq):
    n = len(seq)
    for obj in reversed(seq):
        n -= 1
        yield n, obj


def calculate_profit(stock_values):
    do_buy = np.full(len(stock_values), BUY)  # pass it so save

    profit = 0
    base = 0

    for i, value in reversed_enumerate(stock_values):
        if base <= value:
            do_buy[i] = SELL # set it to sell
            base = value
        profit += base - value
    return profit, do_buy


def calculate_total_money_usage(stock_values, profit, do_buy):
    buy_money = 0
    sell_money = 0
    for stock, action in zip(stock_values, do_buy):
        if action == BUY:
            buy_money += stock
        elif action == SELL:
            sell_money += sell_money
    assert sell_money - buy_money == profit, "Assume that profit calculated right"
    return buy_money, sell_money
