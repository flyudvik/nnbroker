import tensorflow as tf  # Version 1.0 or 0.12
import numpy as np
from os import path

from ..abstract_base import AbstractPredictionWorkflow
from .data_gen import SinusoidDataGenerator


class PredictionWorkFlow(AbstractPredictionWorkflow):
    def __init__(self, f_company):
        self.input_data = []
        self.predictions = []
        self.expected_data = []
        self.bot = f_company.bot
        self.f_rule = f_company

    def _inject_compatibility_code(self):
        try:
            tf.nn.seq2seq = tf.contrib.legacy_seq2seq
            tf.nn.rnn_cell = tf.contrib.rnn
            tf.nn.rnn_cell.GRUCell = tf.contrib.rnn.GRUCell
            print("TensorFlow's version : 1.0 (or more)")
        except:
            print("TensorFlow's version : 0.12")

    def predict(self, *args, **kwargs):
        self._inject_compatibility_code()
        # sess = tf.Session()
        # TODO: hard coded. fuck you
        seq_length = 30
        output_dim = input_dim = 1
        # saver = tf.train.import_meta_graph(self.f_rule.model_meta)
        # saver.restore(sess, save_path=path.dirname(self.f_rule.model_meta))
        # generator = SinusoidDataGenerator()
        # X, Y = generator.get_data_in_date_range(batch_size=1)
        # with tf.variable_scope():
        #     feed_dict = {enc_inp[t]: X[t] for t in range(seq_length)}
        #     outputs = np.array(sess.run([reshaped_outputs], feed_dict)[0])
        return self.hard_mocked()

    def hard_mocked(self):
        self.input_data = map(lambda x: x + 6, [-0.16122538569375169, -0.52875858897410333, -0.46350530886127572, 0.071243116010451524, 0.72094978012678412, 0.97953640689884236, 0.56582731715998458, -0.33640402150036808, -1.1614898101032973, -1.3295229907675739, -0.65339852668498255, 0.50023133004096909, 1.4217068286292076, 1.5130451574326891, 0.68634433996855349])
        self.predictions = map(lambda x: x + 6, [-0.63922626, -1.3297502, -1.151163, -0.32207704, 0.55927098, 0.97392666, 0.73387206, 0.051382948, -0.49906203, -0.5565505, -0.24145573, 0.16098513, 0.37444749, 0.25441071, -0.044709239])
        self.expected_data = map(lambda x: x + 6, [-0.56434049040003409, -1.4724328834664115, -1.4886193769481151, -0.6282355255220422, 0.55096632557825109, 1.3201452303844143, 1.248038154837388, 0.45672054495772252, -0.4947309073086677, -1.0039050446027658, -0.81942449222755243, -0.17166976984126497, 0.43216751730851721, 0.58637706649895971, 0.26347561016929011])
        return self.predictions

