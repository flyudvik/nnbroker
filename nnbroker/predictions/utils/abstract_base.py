import logging

from .decider import calculate_profit, calculate_total_money_usage
from ..exceptions import NotEnoughMoney, MoneyLimitExceeded, ProfitRateDoesNotMatch


logger = logging.getLogger("abstract_workflow")


class AbstractTimeSeriesBuilder(object):
    def get_training_data(self, start, end):
        raise NotImplementedError

    def get_data_in_date_range(self, start, end):
        raise NotImplementedError


class AbstractPredictionWorkflow(object):
    def __init__(self, bot_rules):
        self.bot_rules = bot_rules

    # can be mocked
    def extract_past_stock_data(self, code_name):
        raise NotImplementedError("Should be implemented or mocked")

    def predict_prices(self, past_prices):
        raise NotImplementedError("Should be implemented or mocked")

    def date_parameters(self, code_name):
        raise NotImplementedError("Should be implemented using database")

    # end mock

    def create_strategy(self, rule, bot, prediction, input_data):
        raise NotImplementedError("Should be implemented using database")

    def create_exchange_action(self, strategy, amount, price, action, delay):
        raise NotImplementedError("Should be implemented using database")

    def run(self, *args, **kwargs):
        code_name = self.bot_rules.stock.company_code

        past_prices = self.extract_past_stock_data(code_name)
        date_last, delta_step = self.date_parameters(code_name)

        predicted_prices = self.predict_prices(past_prices)

        predicted_prices = predicted_prices[:self.bot_rules.race_time]

        profit, do_buy = calculate_profit(predicted_prices)
        outcome, income = calculate_total_money_usage(predicted_prices, profit, do_buy)

        if outcome >= self.bot_rules.stock.actual_money:
            raise NotEnoughMoney("Not enough money to buy shares to strategy")

        if outcome >= self.bot_rules.money_spend_limit:
            raise MoneyLimitExceeded("Spendable money limit is exceeded. "
                                     "The limit is too shallow, try to change the limit")

        profit_rate = float(income) / outcome

        if profit_rate < self.bot_rules.expected_profit:
            raise ProfitRateDoesNotMatch("The profit with this strategy should be more")

        # Create strategy
        strategy = self.create_strategy(
            rule=self.bot_rules,
            bot=self.bot_rules.bot,
            prediction=predicted_prices,
            input_data=past_prices
        )
        logger.info("Created strategy {}".format(str(strategy)))

        for i, (price, action) in enumerate(zip(predicted_prices, do_buy)):
            preferred_amount = self.bot_rules.preferred_amount

            # obsolete, because we are interfering the
            # while preferred_amount != 0:
            #     cost = preferred_amount * price
            #     if cost < self.bot_rules.money_spend_limit:
            #         break
            #     preferred_amount -= 1

            # Assign strategy to exchange action
            delay = date_last + delta_step * i

            exchange_action = self.create_exchange_action(
                strategy=strategy,
                amount=preferred_amount,
                price=price,
                action=action,
                delay=delay
            )
        logger.info("Registered {} futures".format(i + 1))
