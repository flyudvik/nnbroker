from django.contrib import admin

from .models import ExchangeBot, AvailableStock, CompanyProcessingRule, ExchangeStrategy, ExchangeAction


class CompanyProcessingRuleAdminInline(admin.TabularInline):
    model = CompanyProcessingRule
    verbose_name = 'rule of following'
    fields = ('stock', 'expected_profit', 'money_spend_limit', 'number_of_allowed_predictions')
    extra = 1


class AvailableStockInlineAdmin(admin.TabularInline):
    model = AvailableStock
    fields = ('company_code', 'company_name')


@admin.register(ExchangeBot)
class BotAdmin(admin.ModelAdmin):
    list_display = ['id', 'owner', 'name', 'period_rate']
    list_display_links = ['id', 'name']
    inlines = [AvailableStockInlineAdmin, ]


@admin.register(AvailableStock)
class AvailableStockAdmin(admin.ModelAdmin):
    inlines = [CompanyProcessingRuleAdminInline, ]
    list_display = ['id', 'bot', 'company_code', 'actual_money']


class ExchangeActionInline(admin.TabularInline):
    model = ExchangeAction


@admin.register(ExchangeStrategy)
class ExchangeStrategyAdmin(admin.ModelAdmin):
    list_display = ['id', 'bot', 'get_company', 'status', 'status_changed']
    inlines = [ExchangeActionInline, ]

    def get_queryset(self, request):
        return super(ExchangeStrategyAdmin, self).get_queryset(request).select_related('stock')

    def get_company(self, obj):
        return obj.rule.stock.company_code
    get_company.title = 'company'
