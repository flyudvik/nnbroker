class PredictionException(Exception):
    pass


class NotEnoughMoney(PredictionException):
    pass


class MoneyLimitExceeded(PredictionException):
    pass


class ProfitRateDoesNotMatch(PredictionException):
    pass
