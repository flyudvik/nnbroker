import os
from django.conf import settings
from django.db import models
from django.utils.functional import cached_property
from model_utils.models import TimeStampedModel, StatusModel
from django.contrib.postgres.fields import JSONField

from .manager import CompanyProcessingRuleManager


class ExchangeBot(TimeStampedModel):
    DEFAULT_RATE = settings.PREDICTIONS_DEFAULT_RATE
    STANDARD_RATE = settings.PREDICTIONS_STANDARD_RATE
    PREMIUM_RATE = settings.PREDICTIONS_PREMIUM_RATE

    RATES = (
        (DEFAULT_RATE, 'default rate'),
        (STANDARD_RATE, 'standard rate'),
        (PREMIUM_RATE, 'premium rate')
    )

    owner = models.ForeignKey(settings.AUTH_USER_MODEL, related_query_name='owned_bots')
    period_rate = models.SmallIntegerField(default=DEFAULT_RATE, choices=RATES)
    name = models.CharField(blank=True, null=True, max_length=128)

    class Meta:
        verbose_name = 'Exchange bot'

    def __str__(self):
        return "BOT#{}".format(self.name)


class AvailableStock(TimeStampedModel):
    bot = models.ForeignKey(ExchangeBot, related_query_name="stocks")
    company_code = models.CharField('RFC exchange name', max_length=20)
    company_name = models.CharField('Full company name', max_length=100)
    amount = models.PositiveIntegerField(default=0, editable=False)
    actual_money = models.FloatField(default=0, editable=False)

    class Meta:
        verbose_name = 'stock'
        default_related_name = 'stocks'
        unique_together = ('bot', 'company_code')

    def __str__(self):
        return "Stock#{} for {}".format(self.id, self.bot.id)

    @cached_property
    def profit(self):
        """
        Calculating the profit
        :return: float
        """
        # TODO: calculate_profit
        # get data from Actions and compare them with actual data on given period
        # get data from TimeSeriesBuilder
        return 0.1


def model_path_with_id(instance, filename):
    return os.path.join(
        "models/{}/{}".format(instance.id, filename)
    )


class CompanyProcessingRule(TimeStampedModel):
    # bot = models.ForeignKey(ExchangeBot, related_name='companies')
    stock = models.ForeignKey(AvailableStock, related_name='rules')
    model_meta = models.FileField(".meta file",
                                  upload_to=model_path_with_id,
                                  name='model.meta',
                                  null=True, blank=True)
    model_index = models.FileField(".index file",
                                   upload_to=model_path_with_id,
                                   name='model.index',
                                   null=True, blank=True)
    model_data = models.FileField(".data file",
                                  upload_to=model_path_with_id,
                                  name='model.data',
                                  null=True, blank=True)

    race_time = models.PositiveIntegerField("Race time",
                                            default=5,
                                            help_text="Number of iterations for each sequence to buy/sell")

    expected_profit = models.FloatField("Percent of expected profit to sell it",
                                        default=1.05,
                                        help_text="1 - no profit, "
                                                  "1.05 default - 5% profit. "
                                                  "Not recommended to use high percent")

    preferred_amount = models.PositiveIntegerField("",
                                                   default=1,
                                                   help_text="Preferred amount of stock share to buy at one time")

    money_spend_limit = models.FloatField("Percent of used money",
                                          default=0,
                                          help_text="If 0, no limit")

    number_of_allowed_predictions = models.SmallIntegerField("Number of linear predictions",
                                                             default=3,
                                                             help_text="If there less 3 linear predictions, "
                                                                       "do not attempt to do anything. "
                                                                       "The higher the number, the higher rate of "
                                                                       "successful trade, but not possible one")

    objects = CompanyProcessingRuleManager()

    class Meta:
        verbose_name = 'following company'
        default_related_name = 'following'

    def __str__(self):
        return 'BOT#{} COMPANY#{}'.format(self.bot.id, self.stock.company_code)

    @cached_property
    def bot(self):
        return self.stock.bot


class ExchangeStrategy(StatusModel, TimeStampedModel):
    RUNNING = 'running'
    FAILED = 'failed'
    COMPLETED = 'completed'
    SUCCEEDED = 'succeeded'

    STATUS = (
        (RUNNING, 'running'),
        (COMPLETED, 'completed'),
        (FAILED, 'failed'),
        (SUCCEEDED, 'succeeded'),
    )

    rule = models.ForeignKey(CompanyProcessingRule, related_name="applied_strategies")
    bot = models.ForeignKey(ExchangeBot, related_name='strategies')

    prediction = JSONField(help_text="Predicted data", null=True, editable=False)
    input_data = JSONField(help_text="Data that used to calculate prediction", null=True, editable=False)
    actual_data = JSONField(help_text="Actual data after prediction", null=True, editable=False)

    class Meta:
        verbose_name = 'Exchange Strategy'

    def __str__(self):
        return "{status} strategy for rule #{rule_id}".format(
            status=self.status,
            rule_id=self.rule
        )


class ExchangeAction(StatusModel, TimeStampedModel):
    SELL = 0
    BUY = 1
    ACTIONS = (
        (BUY, 'buy'),
        (SELL, 'sell'),
    )

    FAILED = 'failed'
    COMPLETED = 'completed'  # bought/ran exchange action
    SUCCEEDED = 'succeeded'
    PENDING = 'pending'
    STATUS = (
        (PENDING, 'pending'),
        (FAILED, 'failed'),
        (SUCCEEDED, 'succeeded'),
        (COMPLETED, 'completed'),
    )

    action = models.PositiveSmallIntegerField(choices=ACTIONS, default=BUY)
    strategy = models.ForeignKey(ExchangeStrategy, related_name='actions')
    delayed_until = models.DateTimeField("")
    amount = models.PositiveSmallIntegerField(default=0)
    price = models.FloatField(default=0)
    actual_price = models.FloatField(default=1, editable=False)

    class Meta:
        verbose_name = 'bot exchange action'
        default_related_name = 'actions'

    def __str__(self):
        return "{action} action for {amount} shares of {price} for {cost}".format(
            action=self.action,
            amount=self.amount,
            price=self.price,
            cost=self.cost,
        )

    @property
    def cost(self):
        return self.price * self.amount

    @property
    def actual_cost(self):
        return self.actual_price * self.amount
