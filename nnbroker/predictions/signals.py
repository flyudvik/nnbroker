import json
import logging

from django.conf import settings
from django.db.models.signals import post_save
from django.dispatch import receiver
# from django_celery_beat.models import IntervalSchedule, PeriodicTask

from .models import CompanyProcessingRule, ExchangeAction

logger = logging.getLogger("predictions.signals")

#
# @receiver(post_save, sender=FollowedCompany)
# def create_new_scheduled_interval_action(sender, instance, created, *args, **kwargs):
#     if not created:
#         return
#     if settings.DEBUG:
#         # TODO: make mock interval schedule
#         pass
#     # TODO: register all interval tasks
#     bot_settings = instance.bot
#     schedule, created = IntervalSchedule.objects.get_or_create(
#         every=bot_settings.period_rate,
#         period=IntervalSchedule.SECONDS,
#     )
#     if created:
#         logger.debug("Created new schedule")
#
#     task, created = PeriodicTask.objects.get_or_create(
#         interval=schedule,
#         name=str(instance),
#         task='nnbroker.predictions.tasks.run_predictions',
#         kwargs=json.dumps({
#             'model': instance.model.path,
#             'instance_id': instance.id,
#             'bot_id': instance.bot.id,
#             'stock_id': instance.stock.id,
#             'debug': settings.DEBUG,
#         })
#     )
#     if created:
#         logger.info("Created new periodic task for {}".format(str(instance)))


@receiver(post_save, sender=ExchangeAction)
def run_transaction(sender, instance, created, *args, **kwargs):
    if not created:
        # Ignore not created data of exchange action
        return
    if settings.DEBUG:
        # TODO: run task run_transaction with NASDAQ API. or mock it
        pass
