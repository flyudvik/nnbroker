from django.db import models


class CompanyProcessingRuleManager(models.Manager):
    def get_queryset(self):
        return super(CompanyProcessingRuleManager, self).get_queryset().select_related('stock')
